#!/bin/sh

# Copyright (c) 2009-2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Script to build valgrind tools (Memcheck, ThreadSanitizer)
# for use with chromium.
sh build-memcheck-for-chromium.sh
sh build-tsan-for-chromium.sh
