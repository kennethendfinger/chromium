/*
 * Copyright 2011, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#import "plugin/mac/oxd_layer.h"

#include "plugin/cross/o3d_glue.h"
#include "plugin/cross/scoped_npobject_ref.h"
#import "plugin/mac/o2d_layer.h"
#import "plugin/mac/o3d_layer.h"
#include "plugin/mac/plugin_mac.h"


@implementation OXDLayerContent

- (id)init {
  self = [super init];

  obj_ = NULL;

  return self;
}

- (void)setPluginObject:(glue::_o3d::PluginObject *)obj {
  obj_ = obj;
}

- (void)setWidth:(int)width height:(int)height {
  width_ = width;
  height_ = height;
  was_resized_ = true;
}

- (BOOL)canDraw {
  // Watch out for the plugin being destroyed out from under us.
  if (!obj_) {
    return NO;
  }

  if (obj_->GetFullscreenMacWindow() != NULL) {
    // Not CA's job to draw when in fullscreen mode; RenderTimer handles that.
    // (We can get here occasionally if the last setNeedsDisplay from before
    // entering fullscreen mode doesn't get handled until after entering
    // fullscreen mode.)
    return NO;
  }

  return YES;
}

- (void)draw {
  o3d::ScopedNPObjectRef obj_ref(obj_);

  if (was_resized_) {
    obj_->Resize(width_, height_);
    was_resized_ = false;
  }

  o3d::Region::RegionSet regions;
  bool have_regions = obj_->renderer()->GetUpdatedRegions(&regions);

  obj_->client()->RenderClient(true);

  if (obj_->drawing_model_ == NPDrawingModelInvalidatingCoreAnimation &&
      !obj_->exiting()) {
    // We need to tell the browser what we updated.
    o3d::IssueNPNInvalidate(obj_, have_regions ? &regions : NULL);
  }
}

@synthesize obj = obj_;

@end


@implementation OXDLayer

- (id)init {
  self = [super init];

  if (self != nil) {
    self.opaque = YES;
    sub_layer_ = NULL;
    o3d_ = YES;
    obj_ = NULL;
  }

  return self;
}

- (void)initSublayer:(BOOL)o3d {
  if (sub_layer_ != nil && o3d_ == o3d) {
    // Already has the right sub-layer.
    return;
  }

  o3d_ = o3d;
  if (sub_layer_ != nil) {
    // Remove existing sub-layers.
    self.sublayers = nil;
    sub_layer_ = nil;
  }

  CALayer* ca_layer;
  if (o3d_) {
    O3DLayer* o3d_layer = [[[O3DLayer alloc] init] autorelease];
    ca_layer = o3d_layer;
    sub_layer_ = o3d_layer;
  } else {
    O2DLayer* o2d_layer = [[[O2DLayer alloc] init] autorelease];
    ca_layer = o2d_layer;
    sub_layer_ = o2d_layer;
  }

  [sub_layer_ setPluginObject:obj_];

  ca_layer.frame = self.frame;
  ca_layer.masksToBounds = YES;
  // Remove undesired animations.
  NSMutableDictionary *nullActions =
      [[NSMutableDictionary alloc] initWithObjectsAndKeys:
          [NSNull null], @"bounds",
          [NSNull null], @"position",
          nil];
  ca_layer.actions = nullActions;
  [nullActions release];
  [self addSublayer:ca_layer];
}

- (void)setNeedsDisplay {
  if (obj_->renderer()) {
    if (obj_->renderer()->SupportsCoreGraphics()) {
      if (o3d_) {
        // Need to create normal CALayer for 2D rendering.
        [self initSublayer:false];
      }
    } else {
      // Must be using O3D.
      DCHECK(o3d_);
    }
  }

  if (sub_layer_) {
    CALayer* layer = static_cast<CALayer*>(sub_layer_);
    layer.frame = self.bounds;
    [layer setNeedsDisplay];
  }
  [super setNeedsDisplay];
}

- (void)setFrame:(CGRect)frame {
  if (sub_layer_) {
    CALayer* layer = static_cast<CALayer*>(sub_layer_);
    [layer setFrame:frame];
  }
  [super setFrame:frame];
}

- (void)setBounds:(CGRect)bounds {
  if (sub_layer_) {
    CALayer* layer = static_cast<CALayer*>(sub_layer_);
    [layer setBounds:bounds];
  }
  [super setBounds:bounds];
}

- (void)setPluginObject:(glue::_o3d::PluginObject *)obj {
  obj_ = obj;
  if (sub_layer_) {
    [sub_layer_ setPluginObject:obj];
  }
}

- (void)setWidth:(int)width height:(int)height {
  if (sub_layer_) {
    [sub_layer_ setWidth:width height:height];
  }
}

@synthesize o3d = o3d_;

@end
