// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef ASH_APP_LIST_APP_LIST_H_
#define ASH_APP_LIST_APP_LIST_H_
#pragma once

#include "base/basictypes.h"
#include "base/compiler_specific.h"
#include "ui/aura/event_filter.h"
#include "ui/aura/root_window_observer.h"
#include "ui/gfx/compositor/layer_animation_observer.h"
#include "ui/views/widget/widget.h"

namespace ash {

class AppListView;

namespace internal {

// AppList is a controller that manages app list UI for shell. To show the UI,
// it requests app list widget from ShellDelegate and shows it when ready.
// While the UI is visible, it monitors things such as app list widget's
// activation state and desktop mouse click to auto dismiss the UI.
class AppList : public aura::EventFilter,
                public aura::RootWindowObserver,
                public ui::ImplicitAnimationObserver,
                public views::Widget::Observer {
 public:
  AppList();
  virtual ~AppList();

  // Show/hide app list window.
  void SetVisible(bool visible);

  // Whether app list window is visible (shown or being shown).
  bool IsVisible();

 private:
  // Sets app list view. If we are in visible mode, start showing animation.
  // Otherwise, we just close it.
  void SetView(AppListView* view);

  // Forgets the view.
  void ResetView();

  // Starts show/hide animation.
  void ScheduleAnimation();

  // aura::EventFilter overrides:
  virtual bool PreHandleKeyEvent(aura::Window* target,
                                 aura::KeyEvent* event) OVERRIDE;
  virtual bool PreHandleMouseEvent(aura::Window* target,
                                   aura::MouseEvent* event) OVERRIDE;
  virtual ui::TouchStatus PreHandleTouchEvent(aura::Window* target,
                                              aura::TouchEvent* event) OVERRIDE;
  virtual ui::GestureStatus PreHandleGestureEvent(
      aura::Window* target,
      aura::GestureEvent* event) OVERRIDE;

  // aura::RootWindowObserver overrides:
  virtual void OnRootWindowResized(const gfx::Size& new_size) OVERRIDE;

  // ui::ImplicitAnimationObserver overrides:
  virtual void OnImplicitAnimationsCompleted() OVERRIDE;

  // views::Widget::Observer overrides:
  virtual void OnWidgetClosing(views::Widget* widget) OVERRIDE;
  virtual void OnWidgetActivationChanged(views::Widget* widget,
      bool active) OVERRIDE;

  // Whether we should show or hide app list widget.
  bool is_visible_;

  // The AppListView this class manages, owned by its widget.
  AppListView* view_;

  DISALLOW_COPY_AND_ASSIGN(AppList);
};

}  // namespace internal
}  // namespace ash

#endif  //  ASH_APP_LIST_APP_LIST_H_
