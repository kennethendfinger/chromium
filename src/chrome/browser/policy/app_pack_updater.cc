// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "chrome/browser/policy/app_pack_updater.h"

namespace policy {

const char AppPackUpdater::kExtensionId[] = "extension-id";
const char AppPackUpdater::kUpdateUrl[]   = "update-url";
const char AppPackUpdater::kOnlineOnly[] = "online-only";

}  // namespace policy
