# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import os
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app


class CommandInvocation(db.Model):
  command_id = db.StringProperty()
  remote_addr = db.StringProperty()
  attempt = db.IntegerProperty()
  retries = db.IntegerProperty()
  cwd = db.StringProperty()
  command = db.StringProperty()
  returncode = db.IntegerProperty()
  stdout = db.StringProperty(multiline=True)
  stderr = db.StringProperty(multiline=True)
  runtime = db.FloatProperty()
  timestamp = db.DateTimeProperty(auto_now_add=True)
  uname_sysname = db.StringProperty()
  uname_nodename = db.StringProperty()
  uname_release = db.StringProperty()
  uname_version = db.StringProperty()
  uname_machine = db.StringProperty()
  uname_processor = db.StringProperty()
  response = db.StringProperty()


class LogHandler(webapp.RequestHandler):
  """Handle requests to log events."""

  def post(self):
    ci = CommandInvocation()
    ci.remote_addr = self.request.remote_addr
    ci.command_id = str(self.request.get('command_id'))
    ci.attempt = int(self.request.get('attempt'))
    ci.retries = int(self.request.get('retries'))
    ci.cwd = str(self.request.get('cwd'))
    ci.command = str(self.request.get('command'))
    ci.returncode = int(self.request.get('returncode'))
    ci.stdout = str(self.request.get('stdout'))
    ci.stderr = str(self.request.get('stderr'))
    ci.runtime = float(self.request.get('runtime'))
    ci.uname_sysname = str(self.request.get('uname_sysname'))
    ci.uname_nodename = str(self.request.get('uname_nodename'))
    ci.uname_release = str(self.request.get('uname_release'))
    ci.uname_version = str(self.request.get('uname_version'))
    ci.uname_machine = str(self.request.get('uname_machine'))
    ci.uname_processor = str(self.request.get('uname_processor'))
    ci.response = '0'
    # Don't retry cat for _index.html
    if (ci.command.find('_index.html') >= 0 and
        ci.command.find('gsutil cat gs://') >= 0 and
        ci.returncode == 1):
      ci.response = '1'
    # Store it.
    ci.put()
    # Emit response.
    self.response.out.write(ci.response)


class ViewerHandler(webapp.RequestHandler):
  """View log info."""

  def get(self):
    user = users.get_current_user()
    if not user:
      uri = self.request.uri
      if uri.startswith('http:'):
        uri = 'https:' + uri[5:]
      self.redirect(users.create_login_url(uri))
      return
    # Only allow @google.com folks to look.
    if not user.email().endswith('@google.com'):
      return
    uname_nodename = self.request.get('uname_nodename')
    if uname_nodename:
      items = db.GqlQuery('SELECT * FROM CommandInvocation '
                          'WHERE uname_nodename = :1 '
                          'ORDER BY timestamp DESC LIMIT 100', uname_nodename)
    else:
      items = db.GqlQuery('SELECT * FROM CommandInvocation '
                          'ORDER BY timestamp DESC LIMIT 100')
    template_values = {
        'items': items,
    }
    path = os.path.join(os.path.dirname(__file__), 'viewer.html')
    self.response.out.write(template.render(path, template_values))


APPLICATION = webapp.WSGIApplication([
    ('/log', LogHandler),
    ('/', ViewerHandler),
], debug=False)


def main():
  run_wsgi_app(APPLICATION)


if __name__ == '__main__':
  main()
