#!/usr/bin/python
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

import os
import socket
import struct
import sys
import time
import threading
import unittest

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from pbrpc import pbrpc_pb2
from pbrpc.stream import Stream

# Access to a protected member XX of a client class
# pylint: disable=W0212

class ChannelMock(object):
  def __init__(self):
    # TODO(hinoka): make this configurable
    self.host = 'localhost'
    self.port = 9000

  def get_port(self, incoming):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    return (sock, (self.host, self.port))

class ContextMock(object):
  def __init__(self, channel):
    self.channel = channel
    self._streams = {}
    self._listening_streams = set()

  def new_stream(self):
    return Stream(self)

  @staticmethod
  def _gen_random(not_in):
    # Used for session_id
    return 1234567890

class testStream(unittest.TestCase):
  def setUp(self):
    self.channel = ChannelMock()
    self.context = ContextMock(self.channel)

  def testListeningStream(self):
    listenstream = self.context.new_stream()
    sid = listenstream.listen()
    self.assertEquals(sid, self.context._gen_random(None))
    sock, sa = self.channel.get_port(False)
    stream_id = listenstream.stream_id
    self.assertTrue(stream_id in self.context._listening_streams)
    self.assertTrue(stream_id in self.context._streams)
    self.assertEquals(self.context._streams[stream_id], listenstream)
    t = threading.Thread(target=listenstream.accept)
    t.start()
    listen_socket = sock
    out_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_socket.bind(sa)
    listen_socket.listen(1)
    out_socket.connect(sa)
    conn, _ = listen_socket.accept()
    listenstream.new_connection(conn)
    self.assertEquals(listenstream.socket, conn)
    time.sleep(0.01)  # Just to make sure accept() is unblocked
    self.assertTrue(not t.is_alive())  # Should've died by now
    self.assertTrue(listenstream.is_open)
    self.assertTrue(stream_id not in self.context._listening_streams)
    reply = out_socket.recv(4096)  # Expecting an ACK
    self.assertTrue(reply)
    packet = pbrpc_pb2.RpcPacket()
    packet.ParseFromString(reply)
    self.assertEquals(packet.stream_id, stream_id)
    self.assertEquals(packet.status, pbrpc_pb2.RpcPacket.ACK)
    # Send some data over
    out_socket.send('Here is some data')
    reply = listenstream.recv()
    # If a string this short fragments, something is definitely wrong
    self.assertEquals(reply, 'Here is some data')
    listenstream.send('Here is a reply')
    self.assertEquals(out_socket.recv(1024), 'Here is a reply')
    out_socket.send('Some string before EOF')
    out_socket.close()
    self.assertEquals(listenstream.recv(), 'Some string before EOF')
    self.assertEquals(listenstream.recv(), None)
    self.assertTrue(not listenstream.is_open)
    self.assertTrue(stream_id not in self.context._streams)
    # Teardown
    listen_socket.close()

  def testConnectingStream(self):
    stream = self.context.new_stream()
    sock, sa = self.channel.get_port(False)
    stream_id = self.context._gen_random(None)
    # Configure sockets
    listen_socket = sock
    listen_socket.bind(sa)
    listen_socket.listen(1)
    # Connect is blocking
    t = threading.Thread(target=stream.connect, args=[stream_id])
    t.start()
    in_socket, _ = listen_socket.accept()
    header = in_socket.recv(4096)
    packet = pbrpc_pb2.RpcPacket()
    packet.ParseFromString(header)
    self.assertEquals(packet.status, pbrpc_pb2.RpcPacket.CONNECT)
    # Send ACK
    packet = pbrpc_pb2.RpcPacket()
    packet.stream_id = 1234567890
    packet.status = pbrpc_pb2.RpcPacket.ACK
    data = packet.SerializeToString()
    header = struct.pack('!I', len(data))
    in_socket.send(header + packet.SerializeToString())
    timeout_counter = 0
    while t.is_alive():
      time.sleep(0.001)
      timeout_counter += 1
      self.assertTrue(timeout_counter < 2000, 'thread is taking too long'
          'something probably broke')
    self.assertTrue(stream.is_open)
    # Teardown
    listen_socket.close()
    in_socket.close()
    stream.close()

  def testListenAndConnectStreamIntegration(self):
    slave_stream = self.context.new_stream()
    client_context = ContextMock(self.channel)
    client_stream = client_context.new_stream()
    stream_id = self.context._gen_random(None)
    slave_stream.listen()
    listen_socket, sa = self.channel.get_port(False)
    listen_socket.bind(sa)
    listen_socket.listen(1)
    # both accept() and connect() block during handshake
    slave_thread = threading.Thread(target=slave_stream.accept)
    slave_thread.start()
    client_thread = threading.Thread(target=client_stream.connect,
        args=[stream_id])
    client_thread.start()
    slave_socket, _ = listen_socket.accept()
    syn = slave_socket.recv(4096)
    self.assertTrue(syn)
    slave_stream.new_connection(slave_socket)
    timeout_counter = 0
    while slave_thread.is_alive() and client_thread.is_alive():
      time.sleep(0.001)
      timeout_counter += 1
      self.assertTrue(timeout_counter < 2000, 'threads are taking too long'
          'something probably broke')
    self.assertTrue(client_stream.is_open)
    self.assertTrue(slave_stream.is_open)
    # Send data
    client_stream.send('Client sending data')
    self.assertEquals(slave_stream.recv(), 'Client sending data')
    slave_stream.send('Slave sending data')
    self.assertEquals(client_stream.recv(), 'Slave sending data')
    # Send data and close stream
    slave_stream.send('Data before EOF')
    slave_stream.close()
    self.assertEquals(client_stream.recv(), 'Data before EOF')
    self.assertEquals(client_stream.recv(), None)
    self.assertTrue(not slave_stream.is_open)
    self.assertTrue(not client_stream.is_open)
    listen_socket.close()


if __name__ == '__main__':
  unittest.main()
