#!/usr/bin/env python
# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Loads a json manifest to define the dependencies and how to run an
executable, usually a test.

The manifest can have dependency on other manifests.
"""

import json
import logging
import optparse
import os
import sys


class DependencyFile(object):
  """A single file that needs to be mapped."""
  def __init__(self, relpath, indir, outdir):
    self.relpath = relpath
    self.inpath = os.path.join(indir, relpath)
    self.outpath = os.path.join(outdir, relpath)

  def __cmp__(self, other):
    """Used for sorting."""
    return cmp(self.relpath, other.relpath)


class Manifest(object):
  """Loads a manifest file and all its dependencies.

  The manifest describes the runtime dependencies for an executable.

  The format is:
  {
    "dependencies": [ <dependencies by name> ],
    "executable": <relative input file name>,
    "files": [ <relative input file names> ],
    "prefix": <common prefix to add to inputs and outputs>,
    "os": {
      <os name>: {
        "dependencies": [ <os-specific dependencies> ],
        "files": [ <os-specific inputs> ]
      }
    }
  }

  All entries are optional.

  <os name> is one of 'linux', 'mac' or 'win32'.
  """
  # Converts sys.platform to expected os names.
  os_map = {
    'cygwin': 'win32',
    'darwin': 'mac',
    'linux2': 'linux',
  }
  # The current platform.
  platform = os_map.get(sys.platform, sys.platform)

  def __init__(self, filename_or_data, indir, outdir):
    """Manifest filename or raw dict, input base directory, output base
    directory.

    Variables are not evaluated here but in _lazy_init().
    """
    # public properties.
    self.variables = {
      'EXE': {'win32': '.exe'}.get(self.platform, ''),
      'DLL': {'win32': '.dll'}.get(self.platform, '.so'),
    }
    self.filename = None
    self.indir = os.path.abspath(indir)
    assert os.path.isdir(self.indir), self.indir
    self.outdir = outdir
    assert self.outdir

    # All late initialized. self._executable can be None if no executable is
    # defined in the dict.
    self._executable = None
    self._dependencies = None
    self._files = None
    self._tree_files = None
    self.common_prefix = None

    # Loads the dict.
    if isinstance(filename_or_data, basestring):
      try:
        self.data = json.load(open(filename_or_data, 'r'))
        self.basedir = os.path.dirname(os.path.abspath(filename_or_data))
        self.filename = filename_or_data
      except ValueError, e:
        e.args = tuple(list(e.args) + [filename_or_data])
        raise
    elif isinstance(filename_or_data, dict):
      self.data = filename_or_data.copy()
      self.basedir = '.'
    else:
      assert False

  def _lazy_init(self):
    """Loads settings from self.data.

    Initializes self._executable, self._files and self._dependencies.
    """
    if not self._files is None:
      return

    self.common_prefix = self.data.get('prefix', '')
    if 'executable' in self.data:
      self._executable = self._new_dep_file(self.data['executable'])
    self._files = [
      self._new_dep_file(i) for i in self.data.get('files', [])
    ]
    self._dependencies = [
      self.__class__(
          os.path.join(self.basedir, i) + '.json', self.indir, self.outdir)
      for i in self.data.get('dependencies', [])
    ]

    # Then do the same for os-specific values.
    os_data = self.data.get('os', {}).get(self.platform, {})
    if 'executable' in os_data:
      # Replaces the executable.
      self._executable = self._new_dep_file(os_data['executable'])
    self._files.extend(
        [self._new_dep_file(i) for i in os_data.get('files', [])])
    self._dependencies.extend([
      self.__class__(
          os.path.join(self.basedir, i) + '.json', self.indir, self.outdir)
      for i in os_data.get('dependencies', [])
    ])
    if self._executable:
      self._files.append(self._executable)

  def _new_dep_file(self, filename):
    """Returns a DependencyFile for filename."""
    filename = os.path.join(self.common_prefix, filename)
    return DependencyFile(filename % self.variables, self.indir, self.outdir)

  @property
  def executable(self):
    """None is a valid value if this is a 'dependency-only' manifest."""
    self._lazy_init()
    return self._executable

  @property
  def dependencies(self):
    """Dependencies of this manifest."""
    self._lazy_init()
    return self._dependencies

  @property
  def files(self):
    """Lists all the inputfiles from this manifests and its dependencies.

    Create it on first request.
    """
    if self._tree_files is None:
      self._lazy_init()
      self._tree_files = self._files[:]
      for i in self._dependencies:
        self._tree_files.extend(i.files)
      self._tree_files.sort()
    return self._tree_files


class ChromiumManifest(Manifest):
  """Chromium-style projects."""

  def __init__(self, filename_or_data, indir, outdir):
    """Adds variables."""
    super(ChromiumManifest, self).__init__(
        filename_or_data,
        indir,
        outdir)
    self.variables.update({
      # Default output directory.
      # TODO(maruel): Do not hardcode Debug.
      'OUTDIR': {
        'linux': 'out',
        'mac': 'xcodebuild',
        'win32': 'build',
      }[self.platform] + '/Debug',
      'PAK': {'win32': '.dll'}.get(self.platform, '.pak'),
    })


def main():
  parser = optparse.OptionParser(
      usage='%prog [options] manifest_file [indir] [outdir]',
      description=sys.modules[__name__].__doc__)
  parser.add_option(
      '-c', '--chromium', action='store_true',
      help='Use chromium-style manifest')
  parser.add_option('-v', '--verbose', action='count')
  options, args = parser.parse_args()
  if options.verbose == 2:
    level = logging.DEBUG
  elif options.verbose == 1:
    level = logging.INFO
  else:
    level = logging.ERROR
  logging.basicConfig(level=level)

  if not args:
    parser.error('Specify a manifest to dump')
  indir = '.'
  outdir = '.'
  if len(args) > 1:
    indir = args[1]
  if len(args) > 2:
    outdir = args[2]
  if options.chromium:
    manifest = ChromiumManifest(args[0], indir, outdir)
  else:
    manifest = Manifest(args[0], indir, outdir)
  for i in manifest.files:
    inpath = os.path.relpath(i.inpath)
    if i.inpath.endswith('/'):
      # Trailing / is removed by relpath().
      inpath += '/'
    print '%s -> %s' % (inpath, i.outpath)
  if manifest.executable:
    print 'Executable: %s' % manifest.executable.outpath
  return 0


if __name__ == '__main__':
  sys.exit(main())
