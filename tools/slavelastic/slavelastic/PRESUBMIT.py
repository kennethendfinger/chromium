# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Top-level presubmit script for slavelastic.

See http://dev.chromium.org/developers/how-tos/depottools/presubmit-scripts for
details on the presubmit API built into presubmit.
"""

import sys

UNIT_TESTS = [
  # Uncomment this as soon as the test is fixed.
  #'slavelastic_tests.slave_test',
]


def CommonChecks(input_api, output_api):
  output = []
  black_list = list(input_api.DEFAULT_BLACK_LIST) + [r'.*_pb2\.py$']
  #+ [r'^slavelastic_tests/.*']

  old_sys_path = sys.path
  try:
    sys.path = [
        input_api.os_path.join(input_api.PresubmitLocalPath(), '..', 'pbrpc')
    ] + sys.path
    output.extend(input_api.canned_checks.RunPylint(
        input_api, output_api, black_list=black_list))
  finally:
    sys.path = old_sys_path
  output.extend(input_api.canned_checks.RunPythonUnitTests(
      input_api,
      output_api,
      UNIT_TESTS))
  return output


def CheckChangeOnUpload(input_api, output_api):
  return CommonChecks(input_api, output_api)


def CheckChangeOnCommit(input_api, output_api):
  return CommonChecks(input_api, output_api)
