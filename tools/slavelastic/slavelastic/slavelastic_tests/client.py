#!/usr/bin/python
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Exercise the slave."""

import logging
import optparse
import os
import sys

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from pbrpc.channel import TcpChannel
from pbrpc.rpc_server import RpcContext, RpcServiceStub
from slavelastic.common.reg import Reg
from slavelastic.common.slavelastic_pb2 import SlaveServiceCore_Stub


def callback(request, response):
  print "Got %s" % response.name


def Do(service, request):
  print "Request: %s" % str(request)
  #response = service.Do(request, callback=callback)
  response = service.Do(request, timeout=10000)
  print "Got %s" % str(response)


def main(argv):
  parser = optparse.OptionParser()
  parser.add_option('-p', '--port', default=9000, type=int)
  parser.add_option('--host', default='127.0.0.1')
  parser.add_option('-v', '--verbose', action='store_true')
  options, args = parser.parse_args()
  level = None
  if options.verbose:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  if args:
    parser.error('Unsupported args: %s' % ' '.join(args))
  context = RpcContext(TcpChannel(options.host, options.port))
  service = RpcServiceStub(context, None, SlaveServiceCore_Stub)
  r = service.GetRegister(Reg(None, 'reg1'))
  print "Got %s" % r
  r = service.SetRegister(Reg('foo', 'reg1'))
  print "Got %s" % r
  r = service.GetRegister(Reg(None))
  print "Got %s" % r


if __name__ == '__main__':
  sys.exit(main(sys.argv))
