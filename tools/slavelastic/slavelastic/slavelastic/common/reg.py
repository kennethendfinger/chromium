# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

from slavelastic.common.slavelastic_pb2 import Register

# Class 'Register' has no 'XX' member
# pylint: disable=E1101


def Reg(obj, name=None, stream=False):
  r = None
  if stream:
    assert isinstance(obj, long), 'stream_id is not an long'
    r = Register(type=Register.STREAM, stream_id=obj)
  elif isinstance(obj, basestring):
    r = Register(type=Register.STRING, string_value=obj)
  elif isinstance(obj, int):
    r = Register(type=Register.STRING, string_value=str(obj))
  elif obj == None:
    r = Register(type=Register.NONE)
  elif isinstance(obj, Exception):
    r = Register(type=Register.EXCEPTION, string_value=str(obj))
  else:
    raise ValueError('Invalid object type', obj)
  if name:
    r.name = name
  return r
