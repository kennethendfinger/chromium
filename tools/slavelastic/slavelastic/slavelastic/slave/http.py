# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""HTTP interface for slaves"""

from __future__ import with_statement
import BaseHTTPServer
import logging
import os
import urllib

try:
  import json
except ImportError:
  import simplejson as json

def _run_http(impl):
  index_file = os.path.join(os.path.dirname(__file__), 'index.html')
  class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_HEAD(self):
      self.send_response(200)
      self.send_header('Content-type', 'text/html')
      self.end_headers()

    def do_POST(self):
      """So far only used for uploading a new slave.py"""

    def do_GET(self):
      self.send_response(200)
      if self.path == '/json':
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        result = {}
        result['registers'] = [(key, value.string_value) for key, value in
            impl.registers.iteritems()]
        result['streams'] = [item[1] for item in impl.streams]
        result['procs'] = [(name, proc.poll()) for name, proc
            in impl.processes.iteritems()]
        result['closed'] = [item for item in impl.closed_processes]
        self.wfile.write(json.dumps(result))
        return
      if self.path == '/unlock':
        impl.update_register('lock', 'Unlocked')
      if self.path.startswith('/update'):
        if len(self.path.split('/')) == 4:
          _, _, reg_name, reg_value = self.path.split('/')
          if reg_name in impl.registers:
            impl.update_register(reg_name, urllib.unquote(reg_value))
      if self.path == '/out' or self.path == '/err':
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        file_path = os.path.join(os.path.dirname(__file__),
            self.path.strip('/')+'.txt')
        self.wfile.write(open(file_path, 'r').read())
        return
      self.send_header('Content-type', 'text/html')
      self.end_headers()
      with open(index_file, 'r') as fd:
        self.wfile.write(fd.read())

    def log_message(self, *args):
      pass

  try:
    logging.debug('Trying to bind to port 80...')
    httpd = BaseHTTPServer.HTTPServer(('', 80), Handler)
  # Try to bind to ports 8000 to 8010
  except Exception:
    for i in range(8000, 8010):
      try:
        logging.debug('Trying to bind to port %d...' % i)
        httpd = BaseHTTPServer.HTTPServer(('', i), Handler)
      except Exception:
        continue
      else:
        break
  logging.info('HTTP Server started..')
  httpd.serve_forever()
  logging.debug('HTTP Server closing')
  httpd.server_close()
