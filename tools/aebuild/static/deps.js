// This file was autogenerated by calcdeps.py
goog.addDependency("../../console/buildbot.js", ['Buildbot'], ['goog.Timer', 'goog.ui.Component', 'goog.ui.Component.Error', 'BuildbotConfig', 'BuildbotModel', 'ConsoleRenderer', 'ConsoleScroller', 'ConsoleLayout', 'Logger']);
goog.addDependency("../../console/buildbotconfig.js", ['BuildbotConfig'], []);
goog.addDependency("../../console/buildbotdata.js", ['BuildbotData'], ['Revision']);
goog.addDependency("../../console/buildbotmodel.js", ['BuildbotModel'], ['goog.net.XhrIo', 'BuildbotData', 'BuildbotConfig']);
goog.addDependency("../../console/consolelayout.js", ['ConsoleLayout'], ['goog.dom', 'Buildbot']);
goog.addDependency("../../console/consolerenderer.js", ['ConsoleRenderer'], ['goog.dom', 'DomHelper']);
goog.addDependency("../../console/domhelper.js", ['DomHelper'], ['goog.dom']);
goog.addDependency("../../console/logger.js", ['Logger'], ['goog.debug.FancyWindow', 'goog.debug.Logger', 'BuildbotConfig']);
goog.addDependency("../../console/revision.js", ['Revision'], []);
goog.addDependency("../../console/scroll.js", ['ConsoleScroller'], ['goog.dom', 'goog.events', 'goog.events.Event', 'Buildbot']);
