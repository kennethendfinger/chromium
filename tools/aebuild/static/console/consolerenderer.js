// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('ConsoleRenderer');

goog.require('goog.dom');

goog.require('DomHelper');

/**
 * The renderer for the Buildbot. In the near future, this is made to allow
 * abstractions between different renderers.
 * @constructor
 */
ConsoleRenderer = function(model) {
  this.model_ = model;
};

/**
 * The build bot model.
 * @return {BuildbotModel} the model for buildbot.
 */
ConsoleRenderer.prototype.getModel = function() {
  return this.model_;
};


/**
 * Creates a DOM for component as a Buildbot.
 */
ConsoleRenderer.prototype.createDom = function() {
  var table = goog.dom.createDom('table',
      {'id':'console', 'style': 'width: 98%'});
  var projects = goog.dom.createDom('tr', {'id':'projects'});
  var projectsContents = goog.dom.createDom('td', null,
      'Welcome to the new console view, Ajax Style!');
  var categories = goog.dom.createDom('tr', {'id':'categories'});
  var categoriesContents = goog.dom.createDom('td');
  var builders = goog.dom.createDom('tr', {'id':'builders'});
  var buildersContents = goog.dom.createDom('td', null,
      'Watch it magically get data!');
  
  goog.dom.appendChild(table, projects);
  goog.dom.appendChild(projects, projectsContents);
  goog.dom.appendChild(table, categories);
  goog.dom.appendChild(categories, categoriesContents);
  goog.dom.appendChild(table, builders);
  goog.dom.appendChild(builders, buildersContents);
  
  return table;
};


/**
 * Decorates an existing HTML element as a Buildbot.
 * @param {Element} element The table element to decorate.
 */
ConsoleRenderer.prototype.decorateDom = function(element) {
  logger.info('Document Decorated');
};


/**
 * Called when the DOM for the component is for sure in the document.
 */
ConsoleRenderer.prototype.enterDocument = function() {
  logger.info('Document Loaded');
};

/**
 * Called when the DOM for the component is for sure in the document.
 */
ConsoleRenderer.prototype.exitDocument = function() {
  logger.info('Document UnLoaded');
};

/**
 * Display a line containing the information about a revision. If the revision
 * line already exists, it will replace it.
 * The line (and the possible delete), won't be committed to screen until the
 * domhelper.commit function is called.
 * @param {Revision} revisionData The revision data json object.
 * @param {boolean} top Place the dom to the top of the builders.
 */
ConsoleRenderer.prototype.displayRevision = function(revisionData, top) {
  // Create the TR for this revision.
  var revLine = DomHelper.TR('ConsoleRevisionLine', revisionData.getRevision());
  
  var revTd = goog.dom.createDom('td', 'ConsoleRevisionRevision',
                                  revisionData.getRevision());
  var nameTd = goog.dom.createDom('td', 'ConsoleRevisionName',
                                  revisionData.getWho());
  
  goog.dom.appendChild(revLine, revTd);
  goog.dom.appendChild(revLine, nameTd);
  
  //TODO(mhm) Use buildbotdata instead.
  var builders = this.getModel().builders_;
  var buildersCount = this.getModel().buildersCount_;

  // Display all builders.
  for (project in builders) {
    for (category in builders[project]) {
      // Create a new td for this category.         
      var categoryTd = DomHelper.TD('ConsoleRevisionCategory', '',
          buildersCount[project][category].percentage + '%',
          1,  // No colspan.
          '');

      // Append those categories to the appropriate line.
      goog.dom.appendChild(revLine, categoryTd);
      
      // We have a subtable for the builders.
      var builderTable = DomHelper.appendTable(categoryTd);

      for (builder in builders[project][category]) {
        // Create the TD.
        var builderTD = DomHelper.TD('ConsoleRevisionStatus', null, null, 1,
                                     '');
        var status = revisionData.getState(project, category, builder);

        // Create the a href.
        var aInfo = {
          'class': 'ConsoleRevisionStatus ' + status,
          'href': '#',
          'target': '_blank'
        };
        var builderA = goog.dom.createDom('a', aInfo);

        goog.dom.appendChild(builderTD, builderA);
        goog.dom.appendChild(builderTable, builderTD);
      }
    }
  }
  
  // Search for the old revision.
  var oldRev = goog.dom.$(revisionData.getRevision());
  if (oldRev) {
    // Replace existing revision.
    DomHelper.appendClass(oldRev, 'pending delete');
    goog.dom.insertSiblingAfter(revLine, oldRev); 
  } else if (top) {
    // Append a new revision to the top.
    goog.dom.insertSiblingAfter(revLine, goog.dom.$('builders')); 
  } else {
    // Append a new revision to the bottom.
    goog.dom.insertSiblingAfter(revLine,
        goog.dom.getLastElementChild(goog.dom.$('console')));
  }

};


/**
 * Display all revisions that need to be updated. (new or modified).
 * The change will take effect only when DomHelper.commit is called.
 */
ConsoleRenderer.prototype.displayRevisions = function() {
  var revisions = this.getModel().getData().getArray();
  var revSize = revisions.length;
  for (var i = 0; i < revSize; i++) {
    var revision = revisions[i];
    
    // Tag for removal within the next domhelper commit since its not visible.
    if (!revision.isVisible()) {
      var lastRev = goog.dom.$(revision.getRevision());
      if (lastRev) {
        DomHelper.appendClass(lastRev, 'pending delete');
      }
    } else if (revision.shouldUpdate()) {
      // The revision has been invalidated, so an update is needed.
      this.displayRevision(revision, true);
      revision.updated();
    } 
  }

  // Commit UI to screen.
  DomHelper.commit();
};


/**
 * Display the top 3 rows showing all the builders/categories/projects.
 * The change will take effect only when DomHelper.commit is called.
 */
ConsoleRenderer.prototype.displayBuilders = function() {
  var projectLine = DomHelper.TR('ConsoleProjects', 'projects');
  var categoryLine = DomHelper.TR('ConsoleCategories', 'categories');
  var builderLine = DomHelper.TR('ConsoleBuilders', 'builders');
   
  // This line starts with 2 hidden td.
  DomHelper.add2TD(projectLine);
  DomHelper.add2TD(categoryLine);
  DomHelper.add2TD(builderLine);

  var td;
  var first = true;
  
  //TODO(mhm) Use buildbotdata instead.
  var builders = this.getModel().builders_;
  var buildersCount = this.getModel().buildersCount_;
  
  // For each project, we create a TD for it.
  for (project in builders) {
    // Take care of the project line first.
    td = DomHelper.TD('ConsoleProject',
                      encodeURIComponent(project),
                      buildersCount[project].percentage + '%',
                      buildersCount[project].count_category,
                      project)

    // Add the special class for the rounding if this is the first project.
    if (first) {
      DomHelper.appendClass(td, 'ConsoleProjectFirst');
      first = false;
    }
    
    // Append this project td to the appropriate line.
    goog.dom.appendChild(projectLine, td);
    
    for (category in builders[project]) {
      // Create the TDs.
      var categoryTdC = DomHelper.TD('ConsoleCategory', '',
          buildersCount[project][category].percentage + '%',
          1,  // No colspan.
          category);
          
      var categoryTdB = DomHelper.TD('ConsoleBuilderStatusCategory', '',
          buildersCount[project][category].percentage + '%',
          1,  // No colspan.
          '');

      // Append those categories to the appropriate line.
      goog.dom.appendChild(categoryLine, categoryTdC);
      goog.dom.appendChild(builderLine, categoryTdB);
      
      // We have a subtable for the builders.
      var builderTable = DomHelper.appendTable(categoryTdB);

      for (builder in builders[project][category]) {
        // Create the TD.
        var builderTD = DomHelper.TD('ConsoleBuilderStatus', null, null,
                                      1, '');

        // Create the a href.
        var aInfo = {
          'class': 'ConsoleBuilderStatus ' +
              builders[project][category][builder].state,
          'href': '#',
          'target': '_blank'
        };
        var builderA = goog.dom.createDom('a', aInfo, '');

        goog.dom.appendChild(builderTD, builderA);
        goog.dom.appendChild(builderTable, builderTD);
      }
    }
  }

  // Add the special class for the rounding if this is the last project.
  DomHelper.appendClass(td, 'ConsoleProjectLast');
  
  // Now we need to insert it into the dom. Since it is pending commit, it
  // wont actually be visible. We also need to set the old one as pending
  // delete, so it will be deleted during the next ui update.
  var oldProjects = goog.dom.$('projects');
  DomHelper.appendClass(oldProjects, 'pending delete');
  goog.dom.insertSiblingAfter(projectLine, oldProjects);

  var oldCategories = goog.dom.$('categories');
  DomHelper.appendClass(oldCategories, 'pending delete');
  goog.dom.insertSiblingAfter(categoryLine, oldCategories);
  
  var oldBuilders = goog.dom.$('builders');
  DomHelper.appendClass(oldBuilders, 'pending delete');
  goog.dom.insertSiblingAfter(builderLine, oldBuilders);
  
  // Commit UI to screen.
  DomHelper.commit();
};