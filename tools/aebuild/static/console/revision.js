// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('Revision');

/**
 * The class contains the data and actions for a single revision.
 */
Revision = function() {
  /**
   * Revision identifier.
   * @type {string}
   * @private
   */
  this.revision_ = "";

  /**
   * Name of the person who committed this revision.
   * @type {string}
   * @private
   */
  this.who_ = "";

  /**
   * Time when this revision was submitted.
   * @type {string}
   * @private
   */
  this.when_ = "";

  /**
   * Comments describing the changes in the revision.
   * @type {string}
   * @private
   */
  this.comments_ = "";

  /**
   * Link pointing to a web page showing the details of this revision.
   * @type {string}
   * @private
   */
  this.revlink_ = "";

  /**
   * Contains the results of the first build including this revision for all
   * builders.
   * @type {dictionary}
   * @private
   */
  this.data_ = {};
  
  /**
   * Timestamp of the last time this revision has been updated.
   * @type {string}
   * @private
   */
  this.modified_ = "";

  /**
   * Flag to know if we should stop trying to update this revision because we
   * have all the data already.
   * @type {boolean}
   * @private
   */
  this.done_ = false;

  /**
   * Flag to know if we should display this revision again.
   * @type {boolean}
   * @private
   */ 
  this.invalidate_ = true;
  
  /**
   * Flag to know if we display this revision or not.
   * @type {boolean}
   * @private
   */ 
  this.visible_ = true;
};


/**
 * Sets the revision information from a dictionary.
 * @param {dictionary} revisionDict The dictionary that contains the info for
 *                                  the revision.
 */  
Revision.prototype.fromDict = function(revisionDict) {
  this.revision_ = revisionDict['revision'];
  this.who_ = revisionDict['who'];
  this.when_ = revisionDict['when']; 
  this.comments_ = revisionDict['comments'];
  this.revlink_ = revisionDict['revlink'];
      
  this.data_[revisionDict['projectname']] = goog.json.parse(revisionDict['data']);
  
  this.modified_ = revisionDict['modified'];
  this.done_ = revisionDict['done'];

  this.invalidate_ = true;
};


/**
 * Invalidates the revision. It will be displayed during the next update.
 */  
Revision.prototype.invalidate = function() {
  this.invalidate_ = true;
};


/**
 * Returns whether the revision should be displayed or not.
 * @return {boolean}
 */  
Revision.prototype.shouldUpdate = function() {
  return this.invalidate_;
};


/**
 * Marks the revision as updated. (Not invalidated)
 */    
Revision.prototype.updated = function() {
  this.invalidate_ = false;
};


/**
 * Returns an array of all the projects we have information for.
 * @return {Array.<string>} Array of all project names.
 */  
Revision.prototype.getProjects = function() {
  var projectArray = [];
  for (project in this.data_) {
    projectArray.push(project);
  }
  
  // Sort projects?
  return projectArray;
};


/**
 * Returns an array of all the categories for a given project.
 * @param project Project containing the categories.
 * @return {Array.<string>} Array of all category names.
 */ 
Revision.prototype.getCategories = function(project) {
  var categoryArray = [];
  if (!this.data_[project]) {
    return categoryArray;
  }
  
  for (category in this.data_[project]) {
    categoryArray.push(category);
  }
  
  // Sort categories.
  return categoryArray;
};


/**
 * Returns an array of all the builders for a given project and category.
 * @param project Project containing the builders.
 * @param category Category containing the builders.
 * @return {Array.<string>} Array of all builder names.
 */ 
Revision.prototype.getBuilders = function(project, category) {
  var builderArray = [];
  if (!this.data_[project] || !this.data_[project][category]) {
    return builderArray
  }
  
  for (builder in this.data_[project][category]) {
    builderArray.push(builder);
  }
  
  // Sort builders?
  return builderArray;
};


/**
 * Returns the state for a given project/category/buidler.
 * @param project Project containing the builder.
 * @param category Category containing the builder.
 * @param builder Builder we are interested in.
 * @return {string} Status of the build.
 */ 
Revision.prototype.getState = function(project, category, builder) {
  if (!this.data_[project] || !this.data_[project][category] ||
      !this.data_[project][category][builder]) {
      return "notstarted";
  }
 
  return this.data_[project][category][builder].state;
};


/**
 * Returns the text for a given project/category/buidler.
 * @param project Project containing the builder.
 * @param category Category containing the builder.
 * @param builder Builder we are interested in.
 * @return {string} Description of the build state.
 */ 
Revision.prototype.getText = function(project, category, builder) {
  if (!this.data_[project] || !this.data_[project][category] ||
      !this.data_[project][category][builder]) {
      return "";
  }
  
  return this.data_[project][category][builder].text;
};


/**
 * Returns the link for a given project/category/buidler.
 * @param project Project containing the builder.
 * @param category Category containing the builder.
 * @param builder Builder we are interested in.
 * @return {string} Link pointing to the build details.
 */ 
Revision.prototype.getBuildLink = function(project, category, builder) {
  if (!this.data_[project] || !this.data_[project][category] ||
      !this.data_[project][category][builder]) {
      return "";
  }
  
  return this.data_[project][category][builder].link;
};


/**
 * Getter for the Revision identifier.
 * @return {string} Revision identifier.
 */ 
Revision.prototype.getRevision = function() {
  return this.revision_;
};


/**
 * Getter for the username who committed the revision.
 * @return {string} Username.
 */  
Revision.prototype.getWho = function() {
  return this.who_;
};


/**
 * Getter for the time when this revision was committed.
 * @return {string} Time.
 */ 
Revision.prototype.getWhen = function() {
  return this.when_;
};


/**
 * Getter for the revision comments.
 * @return {string} Comments.
 */   
Revision.prototype.getComments = function() {
  return this.comments_;
};


/**
 * Getter for the link to the revision.
 * @return {string} Link.
 */   
Revision.prototype.getRevLink = function() {
  return this.revlink_;
};


/**
 * Getter for the last modified time.
 * @return {string} Last modified time.
 */ 
Revision.prototype.getModified = function() {
  return this.modified_;
};


/**
 * Getter for the link to the revision.
 * @return {boolean} true if done.
 */
Revision.prototype.isDone = function() {
  return this.done_;
};


/**
 * Getter for the link to the visible.
 * @return {boolean} true if visible.
 */
Revision.prototype.isVisible = function() {
  return this.visible_;
};


/**
 * Sets the visibility status of the revision.
 * @param {boolean} val true if visisble.
 */
Revision.prototype.setVisible = function(val) {
  this.visible_ = val;
};
