// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('ConsoleScroller');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.events.Event');

goog.require('Buildbot');

/**
 * The model for the buildbot which provides data and logic functionality
 * to buildbot.
 * @param {Buildbot} the controller.
 * @constructor
 */
ConsoleScroller = function(controller) {
  this.controller_ = controller;
};


/**
 * Initiate ajax scroll for the existing window.
 */
ConsoleScroller.prototype.start = function() {
  logger.info('Scroll Add-On Loaded');
  goog.events.listen(window, goog.events.EventType.SCROLL, this.update_, true,
                     this);
};


/**
 * For each scroll update, check if the end has been reached.
 * @param {Object} event The scroll event.
 * @private
 */
ConsoleScroller.prototype.update_ = function(event) {
  var dom_helper = goog.dom.getDomHelper();

  // The total height of the current document.
  var height = dom_helper.getDocumentHeight();

  // The current scroll height position. Note it is relative to the view port.
  var scroll_height = dom_helper.getDocumentScroll().y +
      dom_helper.getViewportSize().height;

  // Once it reached the end, fetch data from app engine.
  if (height == scroll_height) {
    logger.info('Scroll end reached Fetch Data from APP Engine!');
    this.controller_.getMoreRevisions();
  }
};
