#!/usr/bin/env python
# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Enables to do modifications on a live AppEngine instance remotely.

Code copied from http://code.google.com/appengine/articles/remote_api.html
with minor modifications."""

import code
import getpass
import sys

import utils


def connect(app_id, host=None):
  utils.fix_sys_path()

  from google.appengine.ext.remote_api import remote_api_stub
  from google.appengine.api import memcache
  from google.appengine.ext import db

  # Inject utilities into the global namespace.
  from aebuild import models
  import mass_process

  if not host:
    host = '%s.appspot.com' % app_id
  def auth_func():
    return raw_input('Username: '), getpass.getpass('Password: ')
  remote_api_stub.ConfigureRemoteDatastore(app_id, '/remote_api', auth_func,
                                           host)
  print('You can use mass_process utility functions directly:')
  for name in dir(mass_process):
    item = mass_process.__dict__[name]
    if (name.startswith('_') or not callable(item) or
        not getattr(item, '__doc__')):
      continue
    locals()[name] = item
    print('  %-15s: %s' % (name, item.__doc__))
  print('\nAvailable modules:')
  print('  db, aebuild.models as models, memcache\n')
  code.interact('App Engine interactive console for %s' % (app_id,), None,
                locals())


def main(argv=None):
  if not argv:
    argv = sys.argv
  if len(argv) == 1:
    return connect(utils.get_default_app())
  elif len(argv) == 2:
    return connect(argv[1])
  else:
    return connect(argv[1], argv[2])


if __name__ == '__main__':
  sys.exit(main())
