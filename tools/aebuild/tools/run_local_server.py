#!/usr/bin/env python
# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Helper script to fix the path and download the dependencies to start a local
dev_appserver."""

import os
import subprocess
import sys

# Make sure everything necessary is automatically downloaded.
import utils

gae_path = os.path.realpath(os.path.join(utils.BASE_PATH, '..',
                                         'google_appengine'))
# Run the script.
subprocess.call([os.path.join(gae_path, 'dev_appserver.py')] + sys.argv[1:],
                env=utils.get_env())
