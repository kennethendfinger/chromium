# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import time

from django.http import HttpResponse
from google.appengine.api.labs import taskqueue
from google.appengine.ext import db
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError

import logging
import models
import views


def queue_event(request):
  """Queue /task/process-event."""
  try:
    loop_task = taskqueue.Task(url='/task/process-event')
    loop_task.add('eventqueue')
  except taskqueue.TaskAlreadyExistsError:
    pass
  except taskqueue.TombstonedTaskError:
    pass
  finally:
    return HttpResponse('done')


def cleanup_events(request):
  """Cron job that deletes unneeded events."""
  if views.loadConfig().disable_event_clean:
    return HttpResponse('Skipped')
  nb_deleted = 0
  fetch_size = 100
  try:
    query = models.Event.gql('WHERE processed = TRUE')
    events = query.fetch(fetch_size)
    while events:
      db.delete(events)
      nb_deleted += len(events)
      events = query.fetch(fetch_size)
  except DeadlineExceededError:
    logging.info('DeadlineExceededError')
    pass
  except CapabilityDisabledError:
    logging.info('CapabilityDisabledError.')
  finally:
    logging.info('Deleted %d events' % nb_deleted)
    return HttpResponse('too bad')
