# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Blobs utility functions and classes."""

import cgi
import logging

from django.http import Http404, HttpResponse
from google.appengine.ext import blobstore


def ToUtf8(str):
  if str is None or isinstance(str, string):
    return str
  elif isinstance(str, unicode):
    return str.encode('utf-8')
  else:
    raise Http404('Expected a string or None')


class HttpResponseAttachment(HttpResponse):
  """Makes the result a file download if save_as is a string."""

  _CONTENT_DISPOSITION_FORMAT = 'attachment; filename="%s"'

  def __init__(self, *args, **kwargs):
    save_as = kwargs.pop('save_as', None)
    HttpResponse.__init__(self, *args, **kwargs)
    if save_as:
      self['Content-Disposition'] = (
          self._CONTENT_DISPOSITION_FORMAT % ToUtf8(save_as))


class HttpResponseBlobKey(HttpResponseAttachment):
  """Sends a blob-response based on a BlobKey."""
  def __init__(self, blob_key, *args, **kwargs):
    HttpResponseAttachment.__init__(self, *args, **kwargs)
    self[blobstore.BLOB_KEY_HEADER] = str(blob_key)


class HttpResponseBlobInfo(HttpResponseBlobKey):
  """Send a blob-response based on a BlobInfo."""
  def __init__(self, blob_info, *args, **kwargs):
    if kwargs.get('save_as', None) == True:
      kwargs['save_as'] = blob_info.filename
    HttpResponseBlobKey.__init__(self, blob_info.key(), *args, **kwargs)


def get_uploads(request):
  """Get BlobInfo records sent to this handler."""
  if not hasattr(request, '__uploads'):
    # Cache the parsed uploads.
    request.__uploads = {}
    request.META['wsgi.input'].seek(0)
    fields = cgi.FieldStorage(request.META['wsgi.input'], environ=request.META)
    for key in fields:
      field = fields[key]
      if (isinstance(field, cgi.FieldStorage) and
          'blob-key' in field.type_options):
        request.__uploads.setdefault(key, []).append(
            blobstore.parse_blob_info(field))
      else:
        if not key in request.POST:
          logging.info('Adding POST %s: %s' % (key, field.value))
          request.POST[key] = field.value
        else:
          logging.info('Skipping POST %s: %s, was %s' % (key, field.value,
              request.POST[key]))
  return request.__uploads


def get_upload(request, field_name):
  return get_uploads(request).get(field_name, [])
