# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Main entry point for all blobs stored on this GAE instance. Each blob should
be in a subdirectory named after their relevant model entity name."""

import logging
import urllib

from django.http import HttpResponse, HttpResponseRedirect
from google.appengine.ext import blobstore

from views import get_required, post_required

from blobs_utils import get_uploads, get_upload
from blobs_utils import HttpResponseBlobKey, HttpResponseBlobInfo


@get_required
def get_upload_url(request):
  """Returns an url that can be used to POST a file to the BlobStore."""
  new_url = blobstore.create_upload_url('/blobs/on_upload')
  logging.info('Returning %s' % new_url)
  return HttpResponse(new_url, mimetype='text/plain')


@get_required
def upload_form(request):
  """Returns a form that can be used to POST a file to the BlobStore."""
  new_url = blobstore.create_upload_url('/blobs/on_upload')
  logging.info('Returning %s' % new_url)
  form = """<html>
<body>
<form action="%s" method="POST" enctype="multipart/form-data">
  <input type="file" name="file"/><br>
  <input type="submit" value="Upload file">
</form>
</body></html>""" % new_url
  return HttpResponse(form)


@post_required
def on_upload(request):
  # TODO(maruel): Limit who can use this url.
  keys = get_uploads(request)
  logging.info('%s' % keys)
  for blob_info in get_upload(request, 'file'):
    # TODO(maruel): Save the key in an entity.
    logging.info('%s, %d bytes, %s' % (blob_info.filename, blob_info.size,
        blob_info.key()))
  # A redirect is required.
  return HttpResponseRedirect('/blobs/upload_success')


@get_required
def upload_failure(request):
  return HttpResponse('failure')


@get_required
def upload_success(request):
  return HttpResponse('success')


@get_required
def serve(request, resource):
  resource = str(urllib.unquote(resource))
  logging.info('Request for "%s"' % resource)
  blob_info = blobstore.BlobInfo.get(resource)
  logging.info('"%s", %d bytes' % (blob_info.filename, blob_info.size))
  return HttpResponseBlobInfo(blob_info)
  # This is a faster shortcut but we can't log properties like file name.
  #return HttpResponseBlobKey(blobstore.BlobKey(resource))
