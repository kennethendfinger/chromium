# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""AppEngine Configuration."""

import logging
import settings

logging.info('Loading %s from %s', __name__, __file__)

def webapp_add_wsgi_middleware(app):
  if not settings.DEBUG:
    # In production, don't load appstats or firepython.
    return app

  try:
    from appstats import recording
  except ImportError, err:
    logging.info('Failed to import recording: %s', err)
  else:
    app = recording.appstats_wsgi_middleware(app)

  try:
    from firepython import middleware
  except ImportError, err:
    logging.info('Failed to import firepython: %s', err)
  else:
    app = middleware.FirePythonWSGI(app)

  return app

import re

def appstats_normalize_path(path):
  """Custom appstats path normalization."""
  if path.startswith('/user/'):
    return '/user/X'
  if path.startswith('/user_popup/'):
    return '/user_popup/X'
  if path.startswith('/rss/'):
    i = path.find('/', 5)
    if i > 0:
      return path[:i] + '/X'
  return re.sub(r'\d+', 'X', path)
