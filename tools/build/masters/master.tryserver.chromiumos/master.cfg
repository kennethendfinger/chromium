# -*- python -*-
# ex: set syntax=python:

# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import os
import shutil
import subprocess
import sys
import tempfile

# These modules come from scripts/master, which must be in the PYTHONPATH.
from common import chromium_utils
from master import master_utils
from master import slaves_list
from master.cros_try_job_git import CrOSTryJobGit
from master.factory import chromeos_factory

from buildbot.changes.gitpoller import GitPoller

# These modules come from scripts/common, which must be in the PYTHONPATH.
import config

ActiveMaster = config.Master.ChromiumOSTryServer

# This is the dictionary that the buildmaster pays attention to. We also use
# a shorter alias to save typing.
c = BuildmasterConfig = {}

config.DatabaseSetup(c, require_dbconfig=ActiveMaster.is_production_host)

####### CHANGESOURCES

repourl = ActiveMaster.repo_url
if not ActiveMaster.is_production_host:
  # The gitpoller doesn't play well with SSH-AGENT, so for test masters running
  # on a developer's machine, just use https.
  repourl = 'https://git.chromium.org/chromiumos/tryjobs.git'

c['change_source'] = GitPoller(
    repourl=repourl,
    branch='master' if ActiveMaster.is_production_host else 'test',
    workdir=tempfile.mkdtemp(prefix='gitpoller'),
    pollinterval=10)

####### BUILDERS

# ----------------------------------------------------------------------------
# BUILDER DEFINITIONS

# The 'builders' list defines the Builders. Each one is configured with a
# dictionary, using the following keys:
#  name (required): the name used to describe this bilder
#  slavename (required): which slave to use, must appear in c['slaves']
#  builddir (required): which subdirectory to run the builder in
#  factory (required): a BuildFactory to define how the build is run
#  category (optional): it is not used in the normal 'buildbot' meaning. It is
#                       used by gatekeeper to determine which steps it should
#                       look for to close the tree.
#
def _GetBuilders():
  cbuildbot_configs = chromium_utils.GetCBuildbotConfigs()

  new_builders = []
  slaves = slaves_list.SlavesList('slaves.cfg', 'ChromiumOSTryServer')

  for cfg in cbuildbot_configs:
    name = cfg['name']
    buildroot = os.path.join(
        '/b/cbuild',
        '%(type)s_%(branch)s' % {
            'type': 'internal' if cfg['internal'] else 'external',
            'branch': 'master'})

    factory = chromeos_factory.CbuildbotFactory(
        params=name,
        trybot=True,
        crostools_repo=None,
        buildroot=buildroot).get_factory()

    # Set mergeRequests=False here because due to a buildbot bug
    # c['mergeRequests'] is ignored.
    new_builders.append({
        'auto_reboot': ActiveMaster.is_production_host,
        'builddir': name.replace(' ', '-'),
        'category': '1release full|info',
        'factory': factory,
        'name': name,
        'slavenames': slaves.GetSlavesName(builder=name),
        'mergeRequests' : False,
    })

  return new_builders

c['builders'] = _GetBuilders()

####### BUILDSLAVES

# the 'slaves' list defines the set of allowable buildslaves. Each element is a
# tuple of bot-name and bot-password. These correspond to values given to the
# buildslave's mktap invocation.
c['slaves'] = master_utils.AutoSetupSlaves(c['builders'],
                                           config.Master.GetBotPassword())

####### SCHEDULERS

smtp_host = config.Master.smtp if ActiveMaster.is_production_host else 'smtp'
email_footer = """
<strong>Please send bugs and questions to %(reply_to)s.  You can
also reply to this email.</strong>
""" % {'reply_to' : ActiveMaster.reply_to}
c['schedulers'] = []
# Note: CrOSTryJobGit produces merge-able build requests, but the builders we
# create above have mergeRequests turned off.
c['schedulers'].append(CrOSTryJobGit(
    name='cros_try_job_git',
    poller=c['change_source'],
    smtp_host=smtp_host,
    from_addr=ActiveMaster.from_address,
    reply_to=ActiveMaster.reply_to,
    email_footer=email_footer))

####### STATUS TARGETS

# Adds common status and tools to this master.
master_utils.AutoSetupMaster(c, ActiveMaster, order_console_by_time=True,
                             public_html='../master.chromium/public_html',
                             templates=[ '../master.chromiumos/templates',
                                         '../master.chromium/templates'])

# Add a dumb MailNotifier first so it will be used for BuildSlave with
# notify_on_missing set when they go missing.
from buildbot.status import mail
c['status'].append(mail.MailNotifier(
    fromaddr=ActiveMaster.from_address,
    builders=[],
    relayhost=config.Master.smtp,
    lookup=master_utils.UsersAreEmails()))

# Try job result emails.
from master.try_mail_notifier import TryMailNotifier

c['status'].append(TryMailNotifier(
    reply_to=ActiveMaster.reply_to,
    failure_message='TRY FAILED',
    footer=email_footer,
    fromaddr=ActiveMaster.from_address,
    subject="try %(result)s for %(reason)s on %(builder)s",
    mode='all',
    relayhost=smtp_host,
    lookup=master_utils.UsersAreEmails()))


####### PROJECT IDENTITY

# Buildbot master url:
c['buildbotURL'] = 'http://chromegw/p/tryserver.chromiumos/'
