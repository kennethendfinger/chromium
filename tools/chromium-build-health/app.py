# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import datetime
import logging
import os.path

from google.appengine.api import app_identity, channel, mail
from google.appengine.ext import db

from django.utils import simplejson as json


# E-mail address that is going to receive alerts.
ALERT_DESTINATION = '"Pawel Hajdan, Jr." <phajdan.jr@chromium.org>'


# Monkey-patch db.Model to make it easier to JSON-serialize it.
def to_dict(self):
  result = dict([(p, unicode(getattr(self, p))) for p in self.properties()])
  assert 'key' not in result
  result['key'] = str(self.key())
  return result
db.Model.to_dict = to_dict


class Channel(db.Model):
  client_id = db.StringProperty(required=True)


class Builder(db.Model):
  @classmethod
  def to_key_name(cls, packet, payload):
    if 'builderName' in payload:
      builder_name = payload['builderName']
    elif 'build' in payload and 'builderName' in payload['build']:
      builder_name = payload['build']['builderName']
    else:
      raise ValueError('cannot get builder name')
    return '(%s, %s)' % (packet['project'], builder_name)

  timestamp = db.DateTimeProperty(required=True)
  project = db.StringProperty(required=True)
  name = db.StringProperty(required=True)
  state = db.StringProperty(required=True)

  slave = db.StringProperty()
  slave_timestamp = db.DateTimeProperty()


class Slave(db.Model):
  timestamp = db.DateTimeProperty(required=True)
  name = db.StringProperty(required=True)
  connected = db.BooleanProperty(required=True)


class OfflineSlave(db.Model):
  slave_name = db.StringProperty(required=True)
  received_alert = db.DateTimeProperty(required=True)


def string_to_datetime(text):
  logging.debug(text)
  items = text.split('.', 1)
  result = datetime.datetime.strptime(items[0], '%Y-%m-%d %H:%M:%S')
  if len(items) > 1:
    result = result.replace(microsecond=int(items[1]))
  return result


def handle_slave_connected(timestamp, packet, payload):
  def tx_slave():
    slave = Slave.get_by_key_name(payload['slave']['name'])

    # Datastore has more recent state than we do, exit.
    if slave and slave.timestamp >= timestamp:
      return False

    if slave:
      slave.timestamp = timestamp
      slave.connected = payload['slave']['connected']
    else:
      slave = Slave(key_name=payload['slave']['name'],
                    timestamp=timestamp,
                    name=payload['slave']['name'],
                    connected=payload['slave']['connected'])
    slave.put()
    return True
  return db.run_in_transaction(tx_slave)


def handle_slave_disconnected(timestamp, packet, payload):
  def tx_slave():
    slave = Slave.get_by_key_name(payload['slavename'])

    # Datastore has more recent state than we do, exit.
    if slave and slave.timestamp >= timestamp:
      return False

    if slave:
      slave.timestamp = timestamp
      slave.connected = False
    else:
      slave = Slave(key_name=payload['slavename'],
                    timestamp=timestamp,
                    name=payload['slavename'],
                    connected=False)
    slave.put()
    return True
  return db.run_in_transaction(tx_slave)


def handle_builder_changed_state(timestamp, packet, payload):
  def tx_builder():
    key_name = Builder.to_key_name(packet, payload)
    builder = Builder.get_by_key_name(key_name)

    # Datastore has more recent state than we do, exit.
    if builder and builder.timestamp >= timestamp:
      return False

    if builder:
      builder.timestamp = timestamp
      builder.state = payload['state']
    else:
      builder = Builder(key_name=key_name,
                        timestamp=timestamp,
                        project=packet['project'],
                        name=payload['builderName'],
                        state=payload['state'])
    builder.put()
    return True
  return db.run_in_transaction(tx_builder)


def handle_build_started_or_finished(timestamp, packet, payload):
  def tx_builder():
    key_name = Builder.to_key_name(packet, payload)
    builder = Builder.get_by_key_name(key_name)

    if not builder or (builder.slave_timestamp and
                       builder.slave_timestamp >= timestamp):
      return False

    builder.slave = payload['build']['slave']
    builder.slave_timestamp = timestamp
    builder.put()
    return True
  return db.run_in_transaction(tx_builder)


HANDLER_MAP = {
  'slaveConnected': handle_slave_connected,
  'slaveDisconnected': handle_slave_disconnected,
  'builderChangedState': handle_builder_changed_state,
  'buildStarted': handle_build_started_or_finished,
  'buildFinished': handle_build_started_or_finished,
}


def display_on_dynamic_view(timestamp, builder):
  """
  Returns true if the builder should be displayed on dynamically-updated
  status page.
  """
  if builder.state == 'offline':
    return True
  return timestamp - builder.timestamp > datetime.timedelta(hours=3)


def get_current_state():
  timestamp = datetime.datetime.now()
  return json.dumps({
      'timestamp': str(timestamp),
      'builders': [builder.to_dict() for builder in Builder.all()
                   if display_on_dynamic_view(timestamp, builder)],
  })


def process_status_push(packets_json):
  packets = sorted(json.loads(packets_json),
                   key=lambda packet: string_to_datetime(packet['timestamp']))
  has_update = False
  for packet in packets:
    logging.info('Got packet: %r' % packet)
    timestamp = string_to_datetime(packet['timestamp'])
    if packet['event'] in HANDLER_MAP:
      if 'payload' in packet:
        payload = packet['payload']
      elif 'payload_json' in packet:
        payload = json.loads(packet['payload_json'])
      else:
        payload = {}
      this_has_update = HANDLER_MAP[packet['event']](timestamp, packet, payload)
      has_update = has_update or this_has_update

  if has_update:
    for channel_entity in Channel.all():
      channel.send_message(channel_entity.client_id, 'update')


def process_offline_alert(slave_name):
  received_alert = datetime.datetime.now()
  slave = OfflineSlave.get_or_insert(key_name=slave_name,
                                     slave_name=slave_name,
                                     received_alert=received_alert)
  if slave.received_alert == received_alert:
    alert = mail.EmailMessage()
    alert.sender = ('Chromium Build Health <noreply@%s.appspotmail.com>'
        % app_identity.get_application_id())
    alert.subject = 'Offline bot alert: %s' % slave_name
    alert.to = ALERT_DESTINATION
    alert.body = '%s is offline' % slave_name
    alert.send()
